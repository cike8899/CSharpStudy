﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutKeywordDemo.Entity;

namespace OutKeywordDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //string str = "da jia hao";

            //Student s = new Student { Age = 20 };
            //Add(s);
            //Console.WriteLine(s.Age);
            //Console.WriteLine("-----");
            //int sum = 10;
            //Add(ref sum);
            //Console.WriteLine(sum);
            //Console.ReadLine();
            int i;
            AddOut(out i);
            Console.WriteLine(i);
            Console.ReadLine();
        }

        private static void SayHello(string strMessage)
        {

        }

        public static void Add(Student stu)
        {
            stu.Age++;
        }

        public static void Add(ref int i)
        {
            i++;
        }
        public static void AddOut(out int i)
        {
            i = 10;
            i++;
        }
    }
}
