﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutKeywordDemo.Entity
{
    public abstract class BaseModel
    {
        public string getName() {
            return string.Empty;
        }

        public abstract string getValue();
    }
}
