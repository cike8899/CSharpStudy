﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutKeywordDemo.Entity
{
    class SubModel : BaseModel
    {
        public override string getValue()
        {
            return string.Empty;
        }
        protected Object postObj(Func<string, int> func, string key)
        {
            return func(string.Concat(key));
        }
    }
}
